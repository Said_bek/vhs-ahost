<?php
    use yii\helpers\Html;

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">Orginal-Mebel</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-gear"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">
                          Sozlamalar
                      </li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/users/index">
                                            <i class="fa fa-users text-aqua"></i>
                                            <span>Foydalanuvchilar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/branch/index">
                                            <i class="fa fa-bank text-aqua"></i>
                                            <span>Filiallar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/clients/index">
                                            <i class="fa fa-user-secret text-aqua"></i>
                                            <span>Mijozlar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/category/index">
                                            <i class="fa fa-dashboard text-aqua"></i>
                                            <span>Kategoriyalar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/sections/index">
                                            <i class="fa fa-gavel text-aqua"></i>
                                            <span>Bo`limlar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Manager')){ ?>
                                    <li>
                                        <a href="/index.php/team/index">
                                            <i class="fa fa-address-card-o text-aqua"></i>
                                            <span>Ustanovshiklar ro`yxati</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin')){ ?>
                                    <li>
                                        <a href="/index.php/team/schedule">
                                            <i class="fa fa-bar-chart text-aqua"></i>
                                            <span>Ustanovshiklar grafigi</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer')){ ?>
                                    <li>
                                        <a href="/index.php/district/index">
                                            <i class="fa fa-bank text-aqua"></i>
                                            <span>Usta-Bot-Hududlar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer')){ ?>
                                    <li>
                                        <a href="/index.php/trade-district/index">
                                            <i class="fa fa-bank text-aqua"></i>
                                            <span>Mijoz-Bot-Hududlar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                            <?php
                                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer')){ ?>
                                    <li>
                                        <a href="/index.php/sale-district/index">
                                            <i class="fa fa-bank text-aqua"></i>
                                            <span>Sotuv-Bot-Hududlar</span>
                                        </a>
                                    </li>
                                <?php }
                            ?>
                        </ul>
                      </li>
                    </ul>
                  </li>
                <li>
                    <div class="pull-right" style="margin:8px;">
                        <?= Html::a(
                            'Chiqish',
                            ['/index.php/site/logout'],
                            ['data-method' => 'post', 'class' => 'btn btn-primary btn-flat']
                        ) ?>
                    </div>
                </li>
                <li class="sidebarr">
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-edit"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".sidebarr").fadeOut("fast");
    });
</script>
