<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    [
                        'label' => 'Buyurtmalar',
                        'icon' => 'tasks',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Sales') or Yii::$app->user->can('Manager'),
                        'items' => [
                            [
                                'label' => 'Jarayondagi buyurtmalar',
                                'active' => Yii::$app->controller->id == 'orders',
                                'icon' => 'tasks',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Manager'),
                                'url' => ['index.php/orders/index']
                            ],
                            [
                                'label' => 'Buyurtmalar holati',
                                'active' => Yii::$app->controller->id == 'orders',
                                'icon' => 'bolt',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Manager') or Yii::$app->user->can('Sales'),
                                'url' => ['index.php/orders/view']
                            ],
                            [
                                'label' => 'Bitgan buyurtmalar',
                                'active' => Yii::$app->controller->id == 'orders',
                                'icon' => 'flag-checkered',
                                'visible' => Yii::$app->user->can('Admin'),
                                'url' => ['index.php/orders/complete']
                            ],
                            [
                                'label' => 'Buyurtma ma`lumotlari',
                                'active' => Yii::$app->controller->id == 'orders',
                                'icon' => 'file',
                                'visible' => Yii::$app->user->can('Admin'),
                                'url' => ['index.php/orders/order-materials']
                            ],
                            [
                                'label' => 'Majburiy fayllar',
                                'active' => Yii::$app->controller->id == 'required-material-order',
                                'icon' => 'thumb-tack',
                                'visible' => Yii::$app->user->can('Admin'),
                                'url' => ['index.php/required-material-order/index']
                            ],

                        ],
                    ],

                    [
                        'label' => 'Botlar',
                        'icon' => 'fa fa-telegram',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR'),
                        'items' => [
                            [
                                'label' => 'Kroy 3D Bot',
                                'icon' => 'television',
                                'active' => Yii::$app->controller->id == 'bot',
                                'visible' => Yii::$app->user->can('Admin'),
                                'url' => ['index.php/bot/index'],],
                            [
                                'label' => 'Usta bot',
                                'icon' => 'wrench',
                                'active' => Yii::$app->controller->id == 'bot',
                                'visible' => Yii::$app->user->can('Admin'),
                                'url' => ['index.php/bot/master'],],
                            [
                                'label' => 'Oylik-avans boti',
                                'active' => Yii::$app->controller->id == 'salary',
                                'icon' => 'money',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR'),
                                'url' => ['index.php/salary/index']
                            ],
                        ],
                    ],
                   
                    [
                        'label' => 'Bot savollari',
                        'icon' => 'question-circle',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR'),
                        'items' => [
                            [
                                'label' => 'Kunlik hisobot savollari',
                                'active' => Yii::$app->controller->id == 'meetup',
                                'icon' => 'microphone',
                                'url' => ['index.php/meetup/index']
                            ],
                            [
                                'label' => 'Klient uchun savollar',
                                'active' => Yii::$app->controller->id == 'feedback-client',
                                'icon' => 'question',
                                'url' => ['index.php/feedback-client/index']
                            ],
                            [
                                'label' => 'OTK uchun savollar',
                                'active' => Yii::$app->controller->id == 'feedback-user',
                                'icon' => 'question',
                                'url' => ['index.php/feedback-user/index']
                            ],
                        ],
                    ],

                    [
                        'label' => 'Hisobotlar',
                        'icon' => 'bar-chart',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Manager') or Yii::$app->user->can('HR'),
                        'items' => [
                            [
                                'label' => 'Bo`limlar bo`yicha',
                                'icon' => 'bookmark-o',
                                'active' => Yii::$app->controller->id == 'report',
                                'url' => ['index.php/report/index']
                            ],
                            [
                                'label' => 'Usta baholari bo`yicha',
                                'icon' => 'wrench',
                                'active' => Yii::$app->controller->id == 'report',
                                'url' => ['index.php/report/feedback_user']
                            ],
                            [
                                'label' => 'Mijoz baholari bo`yicha',
                                'icon' => 'thumbs-o-up',
                                'active' => Yii::$app->controller->id == 'report',
                                'url' => ['index.php/report/feedback_client']
                            ],
                            [
                                'label' => 'Harakatlar',
                                'icon' => 'edit',
                                'active' => Yii::$app->controller->id == 'report',
                                'url' => ['index.php/events/index']
                            ],
                            [
                                'label' => 'Vazifalar',
                                'active' => Yii::$app->controller->id == 'tasks',
                                'icon' => 'thumb-tack',
                                'url' => ['index.php/tasks/index']
                            ],
                        ],
                    ],

                    [
                        'label' => 'Orginal Mebel Bot',
                        'icon' => 'fa fa-telegram',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Sales') or Yii::$app->user->can('HR') or Yii::$app->user->can('Marketer'),
                        'items' => [
                            [
                                'label' => 'Katalog',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'report',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer'),
                                'url' => ['index.php/orginal-katalog/index']
                            ],
                            [
                                'label' => 'Step katalog',
                                'icon' => 'address-card-o',
                                'active' => Yii::$app->controller->id == 'report',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer'),
                                'url' => ['index.php/orginal-step-katalog/index']
                            ],
                            [
                                'label' => 'Resurs',
                                'icon' => 'wrench',
                                'active' => Yii::$app->controller->id == 'report',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Marketer'),
                                'url' => ['index.php/orginal-step-resurs/index']
                            ],
                            [
                                'label' => 'Video',
                                'icon' => 'youtube-play',
                                'active' => Yii::$app->controller->id == 'video',
                                'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('Sales') or Yii::$app->user->can('HR') or Yii::$app->user->can('Marketer'),
                                'url' => ['index.php/video/index']
                            ],
                        ],
                    ],

                    [
                        'label' => 'Meyos-Original-Usta',
                        'icon' => 'cogs',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Marketer'),
                        'items' => [
                            [
                                'label' => 'Yo`nalishlar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'services',
                                'url' => ['index.php/services/index'],
                            ],
                            [
                                'label' => 'Bo`limlar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'services-types',
                                'url' => ['index.php/services-types/index'],
                            ],
                            [
                                'label' => 'Kompaniyalar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'company',
                                'url' => ['index.php/company/index'],
                            ],
                            [
                                'label' => 'Statistika',
                                'icon' => 'bar-chart',
                                'active' => Yii::$app->controller->id == 'service-statistics',
                                'url' => ['index.php/service-statistics/index'],
                            ],
                            
                        ],
                    ],
                    [
                        'label' => 'Meyos-Original-Mijoz',
                        'icon' => 'diamond',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Marketer'),
                        'items' => [
                            [
                                'label' => 'Kategoriyalar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'trade-services',
                                'url' => ['index.php/trade-services/index'],
                            ],
                            [
                                'label' => 'Kategoriya turlari',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'trade-services-types',
                                'url' => ['index.php/trade-services-types/index'],
                            ],
                            [
                                'label' => 'Kompaniyalar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'trade-company',
                                'url' => ['index.php/trade-company/index'],
                            ],
                            [
                                'label' => 'Statistika',
                                'icon' => 'bar-chart',
                                'active' => Yii::$app->controller->id == 'trade-service-statistics',
                                'url' => ['index.php/trade-service-statistics/index'],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Meyos-Sotuv-Bot',
                        'icon' => 'money',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('Admin') or Yii::$app->user->can('HR') or Yii::$app->user->can('Marketer') or Yii::$app->user->can('Sales'),
                        'items' => [
                            [
                                'label' => 'Kategoriyalar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'sale-services',
                                'url' => ['index.php/sale-services/index'],
                            ],
                            [
                                'label' => 'Kategoriya turlari',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'sale-services-types',
                                'url' => ['index.php/sale-services-types/index'],
                            ],
                            [
                                'label' => 'Kompaniyalar',
                                'icon' => 'image',
                                'active' => Yii::$app->controller->id == 'sale-company',
                                'url' => ['index.php/sale-company/index'],
                            ],
                            [
                                'label' => 'Statistika',
                                'icon' => 'bar-chart',
                                'active' => Yii::$app->controller->id == 'sale-service-statistics',
                                'url' => ['index.php/sale-service-statistics/index'],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
