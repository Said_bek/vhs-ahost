<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\Sections;
use app\models\SectionOrders;
use app\models\SectionTimes;
use app\models\SectionMinimal;
use app\models\Minimal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectionsController implements the CRUD actions for Sections model.
 */
class SectionsController extends Controller
{
    public function init()
    {
        parent::init();
        if(Yii::$app->user->isGuest){
            $this->redirect('/index.php/site/login');
        }
        
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sections models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sections::find()->orderBy(['order_column' => SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sections model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $section_minimal = SectionMinimal::find()
        ->where([
            'section_id' => $id
        ])
        ->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'section_minimal' => $section_minimal,
        ]);
    }

    /**
     * Creates a new Sections model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sections();
        $section_time = new SectionTimes();

        if ($model->load(Yii::$app->request->post()) && $section_time->load(Yii::$app->request->post())) {
            $model->created_date = date("Y-m-d");
            $model->status = 1;

            if ($model->save()) {
                //                          START ADD EVENT
                $user_id = Yii::$app->user->id;

                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                $userId = $selectUsers->id;

                eventUser($userId, date('Y-m-d H:i:s'), $model->title, "Bo'lim qo'shildi", "Bo'limlar");

                //

                $section_time->section_id = $model->id;
                $section_time->start_date = date('Y-m-d');
                $section_time->end_date = date('9999-12-31');
                $section_time->status = 1;
                if ($section_time->save()) {
                    return $this->redirect(['/index.php/sections/view', 'id' => $model->id]);
                }
                else{
                    pre($section_time->errors);
                }
                
            }
            else{
                pre($model->errors);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'section_time' => $section_time,
        ]);
    }

    /**
     * Updates an existing Sections model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $section_time = SectionTimes::find()
        ->where(['section_id' => $id])
        ->andWhere([
            '<=','start_date',date('Y-m-d')
        ])
        ->andWhere([
            '>=','end_date',date('Y-m-d')
        ])
        ->one();

        if ($model->load(Yii::$app->request->post()) && $section_time->load(Yii::$app->request->post())) {
            $model->created_date = date("Y-m-d");
            $model->status = 1;

            if ($model->save()) {
                //                          START ADD EVENT
                $user_id = Yii::$app->user->id;

                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                $userId = $selectUsers->id;

                eventUser($userId, date('Y-m-d H:i:s'), $model->title, "Bo'lim o'zgartirildi", "Bo'lim");

                //

                $section_time->section_id = $model->id;
                $section_time->start_date = date('Y-m-d');
                $section_time->end_date = date('9999-12-31');
                $section_time->status = 1;
                if ($section_time->save()) {
                    return $this->redirect(['/index.php/sections/view', 'id' => $model->id]);
                }
                else{
                    pre($section_time->errors);
                }
                
            }
            else{
                pre($model->errors);
            }
            
        }

        return $this->render('update', [
            'model' => $model,
            'section_time' => $section_time,
        ]);
    }

    /**
     * Deletes an existing Sections model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $section_orders = SectionOrders::find()
        // ->where(['section_id' => $id])
        // ->one();
        // if (empty($section_orders)) {
        //     $this->findModel($id)->delete();    
        // }
        // else{
        //     Yii::$app->session->setFlash('danger', "Bu bo'limda buyurtmalar mavjud.");
        // }

        

        return $this->redirect(['/index.php/sections/index']);
    }


    public function actionDeleteTime()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['id']) && isset($_GET['section_id'])) {
                $id = intval($_GET['id']);
                $section_id = intval($_GET['section_id']);

                $model = SectionTimes::find()
                ->where([
                    'section_id' => $section_id
                ])
                ->andWhere([
                    '<>','id',$id
                ])
                ->orderBy([
                    'id' => SORT_DESC
                ])
                ->one();

                if (isset($model) && !empty($model)) {
                    $status = false;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->end_date = '9999-12-31';

                        if ($model->save()) {
                            $current_model = SectionTimes::findOne($id);
                            if (isset($current_model) && !empty($current_model)) {
                                $current_model->delete();
                                $status = true;
                            }
                        }

                        if ($status) {
                            $transaction->commit();
                            return ['status' => 'success'];    
                        }
                        
                    } catch (Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                        return ['status' => 'failure'];
                    }
                }
                else{
                    return ['status' => 'failure_not_time'];
                }

            }
            else{
                return ['status' => 'failure'];
            }
        }
    }


    public function actionOpenModalTime()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['id']) && isset($_GET['section_id'])) {
                $id = intval($_GET['id']);

                $model = SectionTimes::findOne($id);


                return [
                    'header' => '<h3>O`zgartish</h3>',
                    'status' => 'success',
                    'content' => $this->renderAjax('open-modal-time.php',[
                        'model' => $model
                    ]),
                ];

            }
            else{
                return ['status' => 'failure'];
            }
        }
    }

    public function actionSaveTime()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['id']) && isset($_GET['time']) && !empty($_GET['start_date']) && isset($_GET['section_id'])) {

                $id = intval($_GET['id']);
                $time = intval($_GET['time']);
                $section_id = intval($_GET['section_id']);
                $start_date = date('Y-m-d',strtotime(date($_GET['start_date'])));


                $model = SectionTimes::findOne($id);

                if (!isset($model) || empty($model)) {
                    return ['status' => 'failure_empty'];
                }

                if (isset($model) && $start_date == $model->start_date) 
                {
                    $model->work_time = $time;
                    if ($model->save()) {
                        return ['status' => 'success'];
                    }
                }
                else if($start_date > $model->start_date){

                    $yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
                    $model->end_date = $yesterday;
                    if ($model->save()) {
                        $new_time = new SectionTimes();
                        $new_time->section_id = $section_id;
                        $new_time->work_time = $time;
                        $new_time->start_date = $start_date;
                        $new_time->end_date = '9999-12-31';
                        $new_time->status = 1;
                        if ($new_time->save()) {
                            return ['status' => 'success'];
                        }
                    }

                    
                }

            }
            else{
                return ['status' => 'failure'];
            }
        }
    }

    public function actionOpenModal()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            if ($_GET['id']) {
                $id = intval($_GET['id']);

                $section_users = SectionMinimal::find()
                ->where([
                    'section_id' => $id
                ])
                ->all();

                $user_arr = [];
                if (!empty($section_users)) {
                    foreach ($section_users as $key => $value) {
                        $user_arr[] = $value->user_id;
                    }    
                }


                
                $users = Users::find()
                ->where([
                    'not in', 'id', $user_arr
                ])
                ->all();

                return [
                    'header' => '<h3>Javobgarlarni shaxs biriktirish</h3>',
                    'status' => 'success',
                    'content' => $this->renderAjax('open-modal.php',[
                        'id' => $id,
                        'users' => $users,
                    ]),
                ];

            }
        }
    }


    public function actionSaveUsers()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            if ($_GET['id']) {
                $id = intval($_GET['id']);
                $users = $_GET['users'];
                $bonus = intval($_GET['bonus']);
                $penalty = intval($_GET['penalty']);

                if (empty($users))
                    return ['status' => 'failure_users'];
                if (empty($bonus)) 
                    return ['status' => 'failure_bonus'];
                if (empty($penalty))
                    return ['status' => 'failure_penalty'];

                $status = false;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $minimal = new Minimal();
                    $minimal->bonus_sum = $bonus;
                    $minimal->penalty_summ = $penalty;
                    if ($minimal->save()) {
                        if (!empty($users)) {
                            foreach ($users as $key => $value) {
                                $model = new SectionMinimal();
                                $model->minimal_id = $minimal->id;
                                $model->section_id = $id;
                                $model->user_id = $value;
                                if ($model->save()) {
                                    $status = 1;
                                }
                                else{
                                    pre($model->errors);
                                }
                            }
                            if ($status) {
                                $transaction->commit();    
                                return ['status' => 'success'];
                            }
                            
                        }

                    }
                    else{
                        pre($minimal->errors);
                    }
                } catch (Exception $e) {
                    return ['status' => 'failure'];
                }
            }
        }
    }


    public function actionDeleteUser()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            if ($_GET['id']) {
                $id = intval($_GET['id']);

                $section_users = SectionMinimal::findOne($id);
                if (isset($section_users) && $section_users->delete()) {
                    return ['status' => 'success'];
                }
                else{
                    return ['status' => 'failure'];
                }
            }
        }
    }

    /**
     * Finds the Sections model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sections the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sections::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
